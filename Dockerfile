FROM node 8-slim

WORKDIR /app

COPY package.json .

RUN npm install

COPY server.js . 

ENTRYPOINT ["node", "server.js" ]

EXPOSE 3000 