# Gitlab 101

Se le da esta app base escrita en nodejs para que usted cree su propio CI/CD Pipeline.


# Hagan Fork!

# Fase 1 - Basic CI

1. Cree su propio [.gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/)
2. Para esta app no usaremos algo mas que no sea Docker asi que cree su Dockerfile (de preferencia que sea multistage builds)
   1. El ENTRYPOINT puede ser `npm start` o `node server.js`
3. Agregue los jobs necesarios para que haga build -> tag (latest) -> push a Gitlab Container Registry.


# Fase 2 - Adding Tests

1. Agregue el job para que haga `npm test`
2. agregue `cache:` para que entre PIPELINES se utilce el mismo cache. (`node_modules`)


# Fase 3 - Deployments

Ahora haremos deploy de nuestra a un Cluster de GKE.

Usaremos "Settings >  CI/CD > Environment Variables"

1. Necesitamos crear un cluster en GKE
2. Crear un service account e incluirlo como variable secreta dentro de Gitlab.
3. Autenticarse con GKE cluster.
4. Crear los manifests de K8 (deployment + service (ingress))
5. Hacer deployment de nuestra app a este nuevo cluster.



<details>
  <summary>Scripts de ayuda</summary>

### Activar el Service Account (SA).

```bash
echo ${GCLOUD_SERVICE_ACCOUNT_KEY} > /tmp/gcloud-service-key.json
gcloud auth activate-service-account --key-file /tmp/gcloud-service-key.json
```


### Auth a Cluster GKE.

```bash
gcloud config set compute/zone ${GCP_COMPUTE_ZONE}
echo "-> config set project to ${GCP_PROJECT_NAME}"
gcloud config set project ${GCP_PROJECT_NAME}
echo "-> container clusters get-credentials ${GKE_CLUSTER_NAME}"
gcloud container clusters get-credentials ${GKE_CLUSTER_NAME}

# KUBECTL
kubectl config view
```

### Login a GCR si quisieramos hacer nuestras imagenes privadas

```bash
echo ${GCLOUD_SERVICE_KEY} > /tmp/gcloud-service-key.json
gcloud auth activate-service-account --key-file /tmp/gcloud-service-key.json

# https://cloud.google.com/container-registry/docs/advanced-authentication#json_key_file
docker login -u _json_key --password-stdin https://${GCR_DOMAIN} < /tmp/gcloud-service-key.json

```
</details>

# Fase 4 - improved CI

Necesitamos atar CI_PIPELINE_ID o algun otro identificador del PIPELINE a nuestras imagenes,
1. agregar TAG mas simbolico
2. incluir dentro del build script `build-arg="VERSION=${TAG}` para que el app tenga esta nueva version.

# Fase 5 - improved CD

1. necesitamos una manera de hacer deployment de nuestras versioned images.
2. Si tu deseas puedes ser creativo en esta parte. (kubectl set image)
3. O para los que deseen pueden utilizar [kustomize](https://github.com/kubernetes-sigs/kustomize/blob/master/examples/image.md)


```bash
# with kubectl
TAG=1.9.1
kubectl set image deployment/nginx-deployment nginx=nginx:${TAG} --record

# wait for deployment to update
kubectl rollout status deployment.v1.apps/nginx-deployment


# with kustomize

TAG_VERSION=1.0.5
kustomize edit set image foo/bar=foo/bar:$TAG_VERSION
```


